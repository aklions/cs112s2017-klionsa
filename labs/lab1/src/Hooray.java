public class Hooray {

    public static void hooray() {
        //start loop with no means of breaking
		while(true) {
            //Prints out Hooray!
			System.out.println("Hooray!");
    	} //while

    } //hooray

    public static void main(String[] args) {
        //Calls the hooray method
		hooray();
    } //main

} //Hooray (class)
