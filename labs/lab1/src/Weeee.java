public class Weeee {

    public static void weeee() {
        //Prints out Weeee!
		System.out.println("Weeee!");
        //Recurs the method
		weeee();
    } //weeee

    public static void main(String[] args) {
        //Calls the weeee method
        weeee();
    } //main

} //Weeee (class)
