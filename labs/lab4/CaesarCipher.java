
public class CaesarCipher {

    public static void main(String args []) {

        char message[] = new char[]{'S','e','c','r','e','t',' ','M','e','s','s','a','g','e'};

        System.out.print("\nThe top secret message being sent today is: ");
        printMessage(message);

        System.out.println("\nMessage being encrypted...\n");
        message = encrypt(message);

        System.out.print("This is the unbreakable encrypted version: ");
        printMessage(message);

        System.out.println("\nDecrypting message for use...\n");
        message = decrypt(message);

        System.out.print("This is the message after being decrypted (You will have to trust me here):");
        printMessage(message);

    } //main


    public static char[] encrypt(char[] a) {
        int temp;

        for(int i=0;i<a.length;i++) {

            //created to make comparisons easier
            temp = (int)a[i];

            //skips over spaces
            if(temp != 32) {
                //deals with letters that roll over
                if(temp > 85 && temp < 91 || temp > 117) {

                    temp -= 21;

               } //if 2

                //encrypts all other characters
               else {

                   temp += 5;
               } //else

                //replaces the letter in the array w/ the encrypted letter
               a[i] = (char)temp;

            } //if 1

        } //for
    return a;

    } //encrypt

    //encrypt() with some numbers changed
    public static char[] decrypt(char[] a) {
        int temp;

        for(int i=0;i<a.length;i++) {

            //created to make comparisons easier
            temp = (int)a[i];

            //skips over spaces
            if(temp != 32) {
                //deals with letters that roll over
                if(temp > 96 && temp < 102 || temp < 70 ) {

                    temp += 21;

               } //if 2

                //encrypts all other characters
               else {

                   temp -= 5;
               } //else

                //replaces the letter in the array w/ the encrypted letter
               a[i] = (char)temp;

            } //if 1

        } //for
    return a;

    } //encrypt


    public static void printMessage(char[] a) {

        for(int i=0; i<a.length;i++) {

            System.out.print(a[i]);

        } //for

        System.out.println();

    } //printMessage

} //class
