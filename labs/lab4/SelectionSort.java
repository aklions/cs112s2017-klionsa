import java.util.Random;

public class SelectionSort {

    public static void main(String args []) {

        //creating array and printing initial array
        int array[] = new int[10];

        createArray(array);

        System.out.println();

        printArray(array);

        //sorts array and prints each step
        for(int count=0;count<array.length;count++) {
            array = sortArray(array, count);

            printArray(array);
        }


    } //main

    public static int[] createArray(int a[]) {

        //entering values into array
        Random r = new Random();

        for(int i=0;i<a.length;i++) {

            a[i] = r.nextInt(51);

        } //for

        return a;

    } //createArray


    public static void printArray(int a[]) {

        //prints each index of array
        for(int i=0;i<a.length;i++) {

            System.out.print(a[i]+" ");

            //adds an additional space if number is single digit
            if(a[i]<10) {
                System.out.print(" ");
            } //if

        } //for

        //spaces out the different steps
        System.out.println();

    } //printArray

    public static int[] sortArray(int a[], int i) {

        //variables declared
        int j = i, temp;

        //
        for(int k=i;k<a.length;k++) {

            if (a[k] < a[j]) {
                j = k;
            }//if

        } //for

        temp = a[i];
        a[i] = a[j];
        a[j] = temp;

        return a;

    } //sortArray

} //class
