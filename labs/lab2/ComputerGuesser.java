import java.util.Scanner;
import java.util.Random;

public class ComputerGuesser{

    private static int uBound, lBound;

    public static void main(String args []){
        do{
            playGame();
        } while (playAgain());
        System.out.println("Thanks for playing!");
    } //main

    public static boolean checkGuess(){
        Scanner scan = new Scanner(System.in);

        int guess = (uBound+lBound)/2;

            System.out.println("I guess the number "+guess+", how did I do: ");
                int input = scan.nextInt();

                if (input == -1) {
                    System.out.println("My guess was too low...");
                    lBound = guess+1;
                    return false;
                } //if
                else if (input == 0) {
                     return true;
                } // else if
                else if(input == 1) {
                    System.out.println("My guess was too high...");
                    uBound = guess;
                    return false;
                } //else if
                else {
                     System.out.println("Please only type -1, 0, or 1.");
                     return checkGuess();
                } // else
            } //checkGuess

    public static void playGame(){
        //sets up the game
        int guess, count = 0;
        boolean correct;
        uBound = 100;
        lBound = 0;


        System.out.println("Think of a number between 1 and 100(inclusive).");
           System.out.println("I am going to guess now, please type the following based on how I did:\nMy guess was too low: -1\nMy guess was correct: 0\nMy guess was too high: 1");

        //playing the game
        do{
            correct = checkGuess();
            count++;
        }while(!correct);

        System.out.println("I did it! I guessed your number in only "+count+" tries!");

    } //playGame

    public static boolean playAgain(){
        Scanner scan = new Scanner(System.in);

        System.out.println("Do you want to play again? <y/n>");
        String again = scan.next().toLowerCase();

        if(again.equals("y")){
            return true;
        } //if
        else if (again.equals("n")){
             return false;
        } //else if
        else{
            System.out.println("Please type either y or n");
            return playAgain();
        } //else

    } //playAgain

} //class
