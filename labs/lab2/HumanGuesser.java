import java.util.Scanner;
import java.util.Random;

public class HumanGuesser{

    private static int number;

    public static void main(String args []){
        do{
            playGame();
        } while (playAgain());
        System.out.println("Thanks for playing!");
    } //main

    public static void createNumber(){
        //creates number for human to guess
        Random r = new Random();

        number = r.nextInt(100)+1;
    } //createNumber

    public static int userGuess(){
        //acquires user input
        Scanner scan = new Scanner(System.in);

        int guess = scan.nextInt();

        //makes sure user input is between bounds
        while (guess < 1 || guess > 100){
            System.out.println("Number was not between 1 and 100 please try again: ");
            guess = scan.nextInt();
        } //while

        return guess;
    } //userGuess

    public static boolean checkGuess(int g){

        if (g == number){
            return true;
        } //if correct guess
        else if (g < number){
            System.out.println("Guess was too low, try again: ");
            return false;
        } //if too low
        else if (g > number){
            System.out.println("Guess was too high, try again: ");
            return false;
        } //if too high

        System.out.println("Error 37: complain to my creator.");
        return false;
    } //checkGuess

    public static void playGame(){
        //sets up the game
        int guess, count = 0;
        boolean correct;

        createNumber();

        System.out.println("Guess a number between 1 and 100(inclusive):");
        //playing the game
        do{

            guess = userGuess();

            correct = checkGuess(guess);
            count++;
        }while(!correct);

        System.out.println("Correct! You guessed my number in only "+count+" tries!");

    } //playGame

    public static boolean playAgain(){
        Scanner scan = new Scanner(System.in);

        System.out.println("Do you want to play again? <y/n>");
        String again = scan.next().toLowerCase();

        if(again.equals("y")){
            return true;
        } //if
        else if (again.equals("n")){
            return false;
        } //else if
        else{
            System.out.println("Please type either y or n");
            return playAgain();
        } //else

    } //playAgain

} //class
