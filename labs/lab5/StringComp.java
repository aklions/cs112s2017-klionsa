

public class StringComp{

    public static void main(String args []) {

        long times[][] = new long[2][4];

        times[0][0] = stringLoop(1000);
        times[1][0] = buildLoop(1000);

        times[0][1] = stringLoop(10000);
        times[1][1] = buildLoop(10000);

        times[0][2] = stringLoop(50000);
        times[1][2] = buildLoop(50000);

        times[0][3] = stringLoop(100000);
        times[1][3] = buildLoop(100000);

        printArray(times);

    } //main

    public static long stringLoop(int N) {

        long start = System.nanoTime();

        String myString = new String();
        for (int i = 0; i < N; i++) {
            myString = myString + "a";
        } //for

        long end = System.nanoTime();

        return (end - start);

    } //stringLoop

    public static long buildLoop(int N) {

        long start = System.nanoTime();

        StringBuilder mySB = new StringBuilder();
        for (int i = 0; i < N; i++) {
            mySB.append("a");
        } //for

        long end = System.nanoTime();

        return (end - start);

    } //buildLoop

    public static void printArray(long[][] a) {

        System.out.println("\n\nN\tString\t\tStringBuilder");
        System.out.println("1000\t"+a[0][0]+"\t\t"+a[1][0]);
        System.out.println("10000\t"+a[0][1]+"\t"+a[1][1]);
        System.out.println("50000\t"+a[0][2]+"\t"+a[1][2]);
        System.out.println("100000\t"+a[0][3]+"\t"+a[1][3]);

    } //printArray

} //class
