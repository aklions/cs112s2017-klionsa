import java.util.LinkedList;
import java.util.Iterator;

public class ListComp{

    private static long a[][] = new long[8][4];
    private static int count = 0;

    public static void main(String args []) {

    arrayLoop(1000);
    listLoop(1000);
    llLoop(1000);
    iteratingllLoop(1000);

        count++;

    arrayLoop(10000);
    listLoop(10000);
    llLoop(10000);
    iteratingllLoop(10000);

        count++;

    arrayLoop(50000);
    listLoop(50000);
    llLoop(50000);
    iteratingllLoop(50000);

        count++;

    arrayLoop(100000);
    listLoop(100000);
    llLoop(100000);
    iteratingllLoop(100000);

    printArray();

    }



    public static void arrayLoop(int N) {

        //testing add
        long start = System.nanoTime();

        int[] myArray = new int[N];
        for (int i = 0; i < N; i++) {
            myArray[i] = i;
        } //for

        long end = System.nanoTime();

        a[0][count] = end - start;

        //testing get
        start = System.nanoTime();

        for (int i = 0; i < N; i++) {
            int something = myArray[i];
        } //for

        end = System.nanoTime();

        a[1][count] = end - start;

    } //arrayLoop



    public static void listLoop(int N) {

        //testing add
        long start = System.nanoTime();

        LinkedList<Integer> myList = new LinkedList<Integer>();
        for (int i = 0; i < N; i++) {
            myList.add(i);
        } //for

        long end = System.nanoTime();

        a[2][count] = end - start;

        //testing get
        start = System.nanoTime();

        for (int i = 0; i < N; i++) {
            int variable = myList.get(i);
        } //for

        end = System.nanoTime();

        a[3][count] = end - start;

    } //listLoop



    public static void llLoop(int N) {

        //testing add
        long start = System.nanoTime();

        LL mySList = new LL();
        for (int i = 0; i < N; i++) {
            mySList.add(i);
        } //for

        long end = System.nanoTime();

        a[4][count] = end - start;

        //testing get
        start = System.nanoTime();

        for (int i = 0; i < N; i++) {
            int somethingElse = mySList.get(i);
        } //for

        end = System.nanoTime();

        a[5][count] = end - start;

    } //llLoop



    public static void iteratingllLoop(int N) {

        //testing add
        long start = System.nanoTime();

        IteratingGenericLL<Integer> myIList = new IteratingGenericLL<Integer>();
        for (int i = 0; i < N; i++) {
            myIList.add(i);
        } //for

        long end = System.nanoTime();

        a[6][count] = end - start;

        //testing get
        start = System.nanoTime();

        Iterator<Integer> iter = myIList.iterator();
        while (iter.hasNext()) {
            int anotherThing = iter.next();
        } //while

        end = System.nanoTime();

        a[7][count] = end - start;

    } //iteratingllLoop



    public static void printArray() {

        System.out.println("\n\nAdding\t\t1000\t\t10000\t\t50000\t\t100000");
        System.out.println("Array\t\t"+a[0][0]+"\t\t"+a[0][1]+"\t\t"+a[0][2]+"\t\t"+a[0][3]);
        System.out.println("Linkedlist\t"+a[1][0]+"\t\t"+a[1][1]+"\t\t"+a[1][2]+"\t\t"+a[1][3]);
        System.out.println("LL\t\t"+a[2][0]+"\t\t"+a[2][1]+"\t\t"+a[2][2]+"\t\t"+a[2][3]);
        System.out.println("IteratingLL\t"+a[3][0]+"\t\t"+a[3][1]+"\t"+a[3][2]+"\t"+a[3][3]);

        System.out.println("\n\nGetting\t\t1000\t\t10000\t\t50000\t\t100000");
        System.out.println("Array\t\t"+a[4][0]+"\t\t"+a[4][1]+"\t\t"+a[4][2]+"\t\t"+a[4][3]);
        System.out.println("Linkedlist\t"+a[5][0]+"\t\t"+a[5][1]+"\t"+a[5][2]+"\t"+a[5][3]);
        System.out.println("LL\t\t"+a[6][0]+"\t\t"+a[6][1]+"\t\t"+a[6][2]+"\t\t"+a[6][3]);
        System.out.println("IteratingLL\t"+a[7][0]+"\t\t"+a[7][1]+"\t\t"+a[7][2]+"\t\t"+a[7][3]);
    } //printArray

}
