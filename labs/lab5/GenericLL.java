public class GenericLL<KEY> {

	private class Node<KEY> {
		private Node<KEY> next;
		private KEY data;

		public Node() {
			next = null;
			data = null;
		} //Node (constructor)

		public Node(KEY d, Node<KEY> n) {
			next = n;
			data = d;
		} //Node (constructor)

		public KEY getData() {
			return data;
		} //getData

		public Node getNext() {
			return next;
		} //getNext

		public void setData(KEY newData) {
			data = newData;
		} //setData

		public void setNext(Node<KEY> newNext) {
			next = newNext;
		} //setNext

	} //Node (class)


	// LL stuff starts here
	private int size;
	private Node head, tail;

	public GenericLL() {
		head = null;
		tail = null;
		size = 0;
	} //GenericLL (constructor)

	public void add(KEY d) {
		Node<KEY> newNode = new Node(d, null); // create Node, add data

		if (size == 0) {
			head = newNode;
			tail = newNode; //update tail reference
			size++; //increment size
			return;
		} //if

		tail.next = newNode; // make tail point to new node
		tail = newNode; // update tail reference
		size++;

	} //add

	public void addHead(KEY d) {
		Node<KEY> newNode = new Node(d, head);
		head = newNode;
		size++;
	} //addHead

	public void add(int index, int d) {

	} //add

	public KEY get(int index) {
		Node<KEY> currentPosition = head;
		int counter = 0;

		// check to make sure the index is less than size
		if (index >= size || index < 0) {
			System.out.println("Hey you gave me bad input -- get");
			return null;
		} //if

		while (counter < index/* && index >= 0 && index < size*/) {
			currentPosition = currentPosition.next;
			counter++;
		} //while

		return currentPosition.data;
	} //get

	public void remove(int index) {
		// 4 cases:  removing head, removing tail, removing single node, regular

		// check to make sure the index is less than size
		if (index >= size || index < 0) {
			System.out.println("Hey you gave me bad input -- remove");
			return;
		} //if

		// 1 - removing head
		if (index == 0) {
			head = head.next;

			// 3 - removing single node
			if (size == 1) {
				tail = null;
			} //if
		} else {
			// 4 - removing a regular node
			Node<KEY> currentLocation, previousLocation;
			currentLocation = head.next;
			previousLocation = head;
			int counter = 1;

			while (counter < index) {
				currentLocation = currentLocation.next;
				previousLocation = previousLocation.next;
				counter++;
			} //while

			previousLocation.next = currentLocation.next;
			//previousLocation.setNext(currentLocation.getNext());

			// 2 - removing tail
			if (index == size-1) {
				tail = previousLocation;
			} //if

		} //if-else

		size--;
	} //remove

	public int size() {
		return size;
	} //size

} //GenericLL (class)
