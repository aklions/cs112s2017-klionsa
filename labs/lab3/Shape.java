public class Shape {

    protected double perimeter, area;

    public double getPerimeter() {
        return perimeter;
    } //getPerimeter

    public double getArea() {
        return area;
    } //getArea

} //class
