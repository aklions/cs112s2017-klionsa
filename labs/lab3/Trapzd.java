
public class Trapzd extends Quad {

    private int width2, side1, side2;

    public Trapzd(int w1, int h, int w2, int s1, int s2) {
        setWidth(w1);
        setHeight(h);
        width2 = w2;
        side1 = s1;
        side2 = s2;
    } //constructor

    public void calculatePerimeter() {
        perimeter = getWidth() + getHeight() + width2 + side1 + side2;
    } //calculatePerimeter

    public void calculateArea() {
        area = getHeight() *(getWidth() + width2)/2;
    } //calculateArea

} //class
