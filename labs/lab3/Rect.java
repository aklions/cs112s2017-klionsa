
public class Rect extends Quad {

    public Rect(double w, double h) {
        setWidth(w);
        setHeight(h);
    } //constructor

    public void calculatePerimeter() {
        perimeter = 2 * (getWidth() + getHeight());
    } //calculatePerimeter

    public void calculateArea() {
        area = getWidth() * getHeight();
    } //calculateArea

} //class
