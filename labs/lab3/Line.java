
public class Line extends Shape {

    private int x1, y1, x2, y2;

    public Line(int a, int b, int c, int d) {

        x1 = a;
        y1 = b;
        x2 = c;
        y2 = d;

    } //constructor

    public void calculatePerimeter() {
        perimeter = 2 * Math.sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
    } //calculatePerimeter

    public void calculateArea() {
        area = 0;
    } //calculateArea

} //class
