
public class Circle extends Round {

    private double radius;

    public Circle(double r) {
        radius = r;
    } //constructor

    public void calculatePerimeter() {
        perimeter = getPI() * 2 * radius;
    } //calculatePerimeter

    public void calculateArea() {
        area = getPI() * radius * radius;
    } //calculateArea

} //class
