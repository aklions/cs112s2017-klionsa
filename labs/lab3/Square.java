
public class Square extends Rect {

    public Square(double s) {
        super(s, s);
    } //constructor

    public void calculatePerimeter() {
        perimeter = getWidth() * 4;
    } //calculatePerimeter

    public void calculateArea() {
        area = getWidth() * getWidth();
    } //calculateArea

}//class
