
public class Quad extends Shape {

    private double width, height;

    public void setWidth(double n) {
        width =n;
    } //setWidth

    public void setHeight(double n) {
        height = n;
    } //setHeight

    public double getWidth(){
        return width;
    } //getWidth

    public double getHeight(){
        return height;
    } //getHeight

} //class
