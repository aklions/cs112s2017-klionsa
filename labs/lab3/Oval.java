
public class Oval extends Round {

    private int major, minor;

    public Oval(int a, int b) {
        major = a;
        minor = b;
    } //constructor

    public void calculatePerimeter() {
        perimeter = getPI() * Math.sqrt((major*major + minor*minor)/2);
    } //calculatePerimeter

    public void calculateArea() {
        area = getPI() * major * minor;
    } //calculateArea
} //class
