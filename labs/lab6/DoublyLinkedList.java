class DoublyLinkedList {

    private static class Node {

        protected int data;
        protected Node next, previous;

        public Node() {
            next = null;
            previous = null;
            data = 0;
        } //Node (constructor)

        public Node(int d, Node n, Node p) {
            next = n;
            previous = p;
            data = d;
        } //Node (constructor)

    } //Node (class)



    // DoublyLinkedList stuff starts here
    private int size;
    private Node head, tail;

    public DoublyLinkedList() {
        head = null;
        tail = null;
        size = 0;
    } //DoublyLinkedList (constructor)

    public int size() {
        return size;
    } //size

    public boolean isEmpty() {
        return (size == 0);
    } //isEmpty

    public void add(int newInt) {

        Node newNode = new Node(newInt, null, tail);

        if (size == 0) {
            head = newNode;
            tail = newNode;
            size++;
            return;
        } //if

        tail.next = newNode;
        tail = newNode;
        size++;


    } //add

    public int get(int index) {
        Node currentPosition = head;
        int counter = 0;

        // check to make sure the index is less than size
        if (index >= size || index < 0) {
            System.out.println("Hey you gave me bad input -- get");
            return -1;
        } //if

        while (counter < index) {
            currentPosition = currentPosition.next;
            counter++;
        } //while

        return currentPosition.data;
    } //get

    public int getFromEnd(int index) {

        Node currentPosition = tail;
        int counter = size-1;

        // check to make sure the index is less than size
        if (index >= size || index < 0) {
            System.out.println("Hey you gave me bad input -- get");
            return -1;
        } //if

        while (counter > index) {
            currentPosition = currentPosition.previous;
            counter--;
        } //while

        return currentPosition.data;
    } //getFromEnd

    public void remove(int index) {

        if (index >= size || index < 0) {
            System.out.println("Hey you gave me bad input -- remove");
            return;
        } //if

        if (index == 0) {

            if(size != 1){
                head = head.next; //removes node from forward list
                head.previous = null; //removes nodes from reverse list
            }

            if(size == 1) {
                head = null;
                tail = null;
            } //if 2
        } //if 1

        else if (index == size-1) {
            tail = tail.previous;
        } //if

        else {
            Node currentLocation, previousLocation;
            currentLocation = head.next;
            previousLocation = head;
            int counter = 1;

            while (counter < index) {
                currentLocation = currentLocation.next;
                previousLocation = previousLocation.next;
                counter++;
            } //while

            previousLocation.next = currentLocation.next;
            currentLocation.next.previous = previousLocation;
            //shamelessly borrowed from LL
        }

        size--;

    } //remove

    public boolean testPreviousLinks() {
        Node current = tail;
        int count = 1;
        while (current.previous != null) {
            current = current.previous;
            count++;
        } //while
        return ((current == head) && (count == size));
    } //testPreviousLinks

} //DoublyLinkedList (class)
